import java.io.File;
import java.util.List;
import model.Candy;
import parser.StAX;

public class Main {

  public static void main(String[] args) {

    File xml = new File("src\\main\\resources\\XML\\candy.xml");
    File xsd = new File("src\\main\\resources\\XML\\candy.xsd");
    List<Candy> candies = StAX.parse(xml, xsd);
    for(Candy c : candies){
      System.out.println(c);
    }


  }
}
