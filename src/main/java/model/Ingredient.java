package model;

public class Ingredient {
  private String name;

  public Ingredient() {
  }

  public Ingredient(String name){
    this.name = name;
  }

  public String getName(){
    return name;
  }

  public String toString(){
    return "Ingredient{name="+name+"}";
  }

}
