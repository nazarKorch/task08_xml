package model;

import java.util.ArrayList;
import java.util.List;

public class Candy {

  private int num;
  private String name;
  private String type;
  private int energy;
  private String production;
  private List<Ingredient> ingredients = new ArrayList<>();

  public Candy() {
  }

  public Candy(int num, String name, String type, int energy, String production,
      List<Ingredient> ingredients) {
    this.num = num;
    this.name = name;
    this.type = type;
    this.energy = energy;
    this.production = production;
    this.ingredients = ingredients;
  }

  public void setNum(int num) {
    this.num = num;
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setType(String type) {
    this.type = type;
  }

  public void setEnergy(int energy) {
    this.energy = energy;
  }

  public void setProduction(String production) {
    this.production = production;
  }
  public void setIngredients(List<Ingredient> ingredients){
    this.ingredients = ingredients;
  }

  public String toString() {

    return "Candy{num=" + num +
        ", name=" + name +
        ", type=" + type +
        ", energy=" + energy +
        ", production=" + production +
        ", ingredients=" + ingredients + "}";
  }

}
