package parser;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import model.Candy;
import model.Ingredient;

public class StAX {

  public static List<Candy> parse(File xml, File xsd) {
    List<Candy> candies = new ArrayList<>();
    Candy candy = null;
    List<Ingredient> ingredients = null;

    XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
    try {
      XMLEventReader xmlEventReader = xmlInputFactory
          .createXMLEventReader(new FileInputStream(xml));
      while (xmlEventReader.hasNext()) {
        XMLEvent xmlEvent = xmlEventReader.nextEvent();
        if (xmlEvent.isStartElement()) {
          StartElement startElement = xmlEvent.asStartElement();
          String name = startElement.getName().getLocalPart();
          switch (name) {
            case "candy":
              candy = new Candy();

              Attribute attribute = startElement.getAttributeByName(new QName("num"));
              if (attribute != null) {
                candy.setNum(Integer.parseInt(attribute.getValue()));
              }
              break;
            case "name":
              xmlEvent = xmlEventReader.nextEvent();
              assert candy != null;
              candy.setName(xmlEvent.asCharacters().getData());
              break;
            case "type":
              xmlEvent = xmlEventReader.nextEvent();
              assert candy != null;
              candy.setType(xmlEvent.asCharacters().getData());
              break;
            case "energy":
              xmlEvent = xmlEventReader.nextEvent();
              assert candy != null;
              candy.setEnergy(Integer.parseInt(xmlEvent.asCharacters().getData()));
              break;

            case "production":
              xmlEvent = xmlEventReader.nextEvent();
              assert candy != null;
              candy.setProduction(xmlEvent.asCharacters().getData());
              break;
            case "ingredients":
              xmlEvent = xmlEventReader.nextEvent();
              ingredients = new ArrayList<>();
              break;
            case "ingredient":
              xmlEvent = xmlEventReader.nextEvent();
              assert ingredients != null;
              ingredients.add(new Ingredient(xmlEvent.asCharacters().getData()));

              break;

          }
        }

        if (xmlEvent.isEndElement()) {
          candy.setIngredients(ingredients);
          EndElement endElement = xmlEvent.asEndElement();
          if (endElement.getName().getLocalPart().equals("candy")) {
            candies.add(candy);
          }
        }
      }
    } catch (Exception e) {
      System.out.println(e.getMessage());
    }
    return candies;
  }

}
