<?xml version="1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  version="1.0">
  <xsl:template match="/">
    <html>
      <body>
        <h2>Candies</h2>
        <table border="1">
          <tr bgcolor="teal">
            <th>Name</th>
            <th>Type</th>
            <th>Energy</th>
            <th>Production</th>
            <th>Ingredients</th>

          </tr>
          <xsl:for-each select="candies/candy">
            <tr>
              <td><xsl:value-of select="name"/></td>
              <td><xsl:value-of select="type"/></td>
              <td><xsl:value-of select="energy"/></td>
              <td><xsl:value-of select="production"/></td>
              <td><xsl:value-of select="ingredient"/></td>

            </tr>
          </xsl:for-each>
        </table>
      </body>
    </html>

  </xsl:template>

</xsl:stylesheet>